# Testing

<p>
 <img src="https://gitlab.com/pascal.cantaluppi/testing/raw/master//img/debug.png" width="100" alt="Debug" />
</p>

## JavaScript Testing

<p>Automated tests speeds up your development flow and gives you a way of quickly identifying issues, breaking changes and side effects.</p>

- <a href="https://academind.com/learn/javascript/javascript-testing-introduction/" target="blank">https://academind.com/learn/javascript/javascript-testing-introduction/</a>
