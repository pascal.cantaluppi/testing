const puppeteer = require("puppeteer");
const { generateText, checkAndGenerate } = require("./util");

test("should output name and age", () => {
  const text = generateText("Pascal", 40);
  expect(text).toBe("Pascal (40 years old)");
});

// test("should output data-less text", () => {
//   const text = generateText("", null);
//   expect(text).toBe(" (null years old)");
// });

test("should generate a valid text output", () => {
  const text = checkAndGenerate("Pascal", 40);
  expect(text).toBe("Pascal (40 years old)");
});

test("testFormWithBrowser", async () => {
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 80,
    args: ["--window-size=800,600"]
  });
  const page = browser.newPage();
  await (await page).goto("file:///C:/data/Learning/Node/testing/index.html");
});
